module.exports = {
  root: true,
  "extends": ["airbnb", "prettier"],
  "plugins": ["prettier"],
  "rules": {
    "prettier/prettier": ["error"] ,
    "no-console" : "off",
    "import/order" : "off",
  },
};
