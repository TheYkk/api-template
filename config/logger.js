const env = require('./getenv');

module.exports = {
  name: env('LOGGER_NAME', 'app-router-ms'),
  level: env('LOGGER_LEVEL', 'debug'),
  useLevelLabels: true,
};
