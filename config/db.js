const env = require('./getenv');

module.exports = {
  uri: env('DB_URI', 'postgres://postgres:ykk@localhost:5432/local_test'),
  client: env('DB_CLIENT', 'pg'),
};
