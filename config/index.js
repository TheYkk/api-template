const db = require('./db');
const app = require('./app');
const logger = require('./logger');
const packagec = require('../package.json');

module.exports = {
  db,
  app,
  logger,
  version: packagec.version,
};
