// ? Get configs
const cfg = require('../config');

// ? File reader
const fs = require('fs');

// ? Set logger configs and init
const log = require('pino')(cfg.logger);

const serverSettings =
  cfg.app.mode === 'dev'
    ? {logger: log}
    : {
        logger: false,
        https: {
          key: fs.readFileSync(cfg.app.ssl_key, 'utf8'),
          cert: fs.readFileSync(cfg.app.ssl_crt, 'utf8'),
        },
      };

// ? Get Http server
const fastify = require('fastify')(serverSettings);

// Declare a route
fastify.get('/apply/:code', (request, reply) => {
  // ? Answer
  reply.send('ok');
});

// Run the server!
fastify.listen(cfg.app.port, err => {
  if (err) throw err;
});
